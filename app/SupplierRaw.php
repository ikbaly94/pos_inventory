<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierRaw extends Model
{
    protected $table = 'supplier_raws';
}

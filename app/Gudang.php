<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gudang extends Model
{
    protected $table = 'gudangs';

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    
    public function barang()
    {
        return $this->hasMany('App\Barang', 'id_gudang');
    }
}

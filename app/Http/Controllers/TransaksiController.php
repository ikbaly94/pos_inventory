<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Barang;
use App\Gudang;
use App\User;
use App\Supplier;
use Auth;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $level = Auth::user()->level;
        if ($level == 1) {
            $trans = Transaksi::orderBy('id_barang', 'Desc')->get();
            $id_barang = Transaksi::all()->pluck('id_barang');
            $brg = Barang::whereIn('id_gudang', $id_barang)->get();
            return view('transaksi.index',compact('trans', 'brg'));
        }
        else{
            $user = Auth::user()->id;
            $gudang = Gudang::where('user_id', $user)->pluck('id');
            $barang = Barang::whereIn('id_gudang', $gudang)->pluck('id');
            $trans = Transaksi::whereIn('id_barang', $barang)->get();
            $brg = Barang::whereIn('id_gudang', $gudang)->get();
            return view('transaksi.index',compact('trans', 'brg'));

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = Auth::user()->level;
        if ($level == 1) {
            
            $barang = Barang::orderBy('id_gudang')->get();
        return view('transaksi.baru', compact('barang'));
        }
        else{
            $user = Auth::user()->id;
            $gudang = Gudang::where('user_id', $user)->pluck('id');
            $barang = Barang::where('id_gudang', $gudang)->get();
        return view('transaksi.baru', compact('barang' ));

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jenis = $request['jenis'];
        $qty = $request['qty'];

        $id_barang = $request['id_barang'];

        if ($jenis == 1) {
            $trans = new Transaksi;
            $trans->jenis = $request['jenis'];
            $trans->klien = $request['kl1'];
            $trans->qty = $request['qty'];
            $trans->id_barang = $request['id_barang'];
            $trans->save();


            $barang = Barang::where('id', $id_barang)->first();
            $stok = $barang->jumlah;
            $laststok = $stok + $qty;
            $barang->jumlah = $laststok;
            $nominal = $barang->harga;
            $lastnominal = $nominal * $laststok;
            $barang->nominal = $lastnominal;
            $barang->update();

            return redirect('transaksi');
        }
        elseif ($jenis == 2) {
            $trans = new Transaksi;
            $trans->jenis = $request['jenis'];
            $trans->klien = $request['kl1'];
            $trans->qty = $request['qty'];
            $trans->id_barang = $request['id_barang'];
            $trans->save();


            $barang = Barang::where('id', $id_barang)->first();
            $stok = $barang->jumlah;
            $laststok = $stok - $qty;
            $barang->jumlah = $laststok;
            $nominal = $barang->harga;
            $lastnominal = $nominal * $laststok;
            $barang->nominal = $lastnominal;
            $barang->update();

            return redirect('transaksi');
        }
        elseif ($jenis == 3) {
            $trans = new Transaksi;
            $trans->jenis = $request['jenis'];
            $trans->klien = $request['kl2'];
            $trans->qty = $request['qty'];
            $trans->id_barang = $request['id_barang'];
            $trans->save();


            $barang = Barang::where('id', $id_barang)->first();
            $stok = $barang->jumlah;
            $laststok = $stok - $qty;
            $barang->jumlah = $laststok;
            $nominal = $barang->harga;
            $lastnominal = $nominal * $laststok;
            $barang->nominal = $lastnominal;
            $barang->update();

            return redirect('transaksi');
        }
        else 
        {
            $trans = new Transaksi;
            $trans->jenis = $request['jenis'];
            $trans->klien = $request['kl1'];
            $trans->qty = $request['qty'];
            $trans->id_barang = $request['id_barang'];
            $trans->save();


            $barang = Barang::where('id', $id_barang)->first();
            $stok = $barang->jumlah;
            $laststok = $stok + $qty;
            $barang->jumlah = $laststok;
            $nominal = $barang->harga;
            $lastnominal = $nominal * $laststok;
            $barang->nominal = $lastnominal;
            $barang->update();

            return redirect('transaksi');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $level = Auth::user()->level;
        if ($level == 1) {
            
            $barang = Barang::orderBy('id_gudang')->get();
        return view('transaksi.baru', compact('barang'));
        }
        else{
            $user = Auth::user()->id;
            $gudang = Gudang::where('user_id', $user)->pluck('id');
            $barang = Barang::where('id_gudang', $gudang)->get();
        return view('transaksi.baru', compact('barang' ));

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

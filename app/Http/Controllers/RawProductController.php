<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RawProduct;
use App\Unit;

class RawProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rawproduct = RawProduct::all();
        return view('rawproduct.index', compact('rawproduct'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $unit = Unit::all();
        return view('rawproduct.baru',compact('unit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rawproduct = new RawProduct;
        $rawproduct->name = $request['name'];
        $rawproduct->unit_id = $request['unit_id'];
        $rawproduct->save();

        return redirect('rawproduct');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rawproduct = RawProduct::find($id);
        $unit= Unit::All();
        return view('rawproduct.edit', compact('rawproduct','unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rawproduct = RawProduct::find($id);
        $unit = Unit::all();
        $rawproduct->name = $request['name'];
        $rawproduct->unit_id = $request['unit_id'];
        $rawproduct->update();

        return redirect('rawproduct');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplierraw;
use App\Supplier;
use App\RawProduct;
class SupplierrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplierraw = Supplierraw::all();
        return view('supplierraw.index',compact('supplierraw'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $supplier = Supplier::all();
        $rawproduct = RawProduct::all();
        return view('supplierraw.baru',compact('rawproduct','supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplierraw = new Supplierraw;
        $supplierraw->supplier_id = $request['supplier_id'];
        $supplierraw->rawproduct_id = $request['rawproduct_id'];
        $supplierraw->save();

        return redirect('supplierraw');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplierraw = Supplierraw::find($id);
        return view('supplierraw.edit',compact('supplierraw'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplierraw = Supplierraw::find($id);
        $supplierraw->supplier_id = $request['supplier_id'];
        $supplierraw->rawproduct_id = $request['rawproduct_id'];
        $supplierraw->update();

        return redirect('supplierraw');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

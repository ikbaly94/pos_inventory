<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Gudang;
use App\Satuan;
use App\Supplier;
use App\User;
use Auth;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $level = Auth::user()->level;
        if ($level == 1) {
            
            $barang = Barang::orderBy('id_gudang')->get();
            return view('barang.index',compact('barang'));
        }
        else{
            $user = Auth::user()->id;
            $gudang = Gudang::where('user_id', $user)->pluck('id');
            $barang = Barang::where('id_gudang', $gudang)->get();
            return view('barang.index',compact('barang'));

        }

        // return [$barang];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gudang = Gudang::all();
        $satuan = Satuan::all();
        $supplier = Supplier::all();
        return view('barang.baru',compact('gudang', 'satuan', 'supplier'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barang = new Barang;
        $barang->kode = $request['kode'];
        $barang->nama = $request['nama'];
        $barang->jenis = $request['jenis'];
        $barang->harga = $request['nominal'];
        $barang->nominal = 0;
        $barang->satuan = $request['satuan'];
        $barang->id_gudang = $request['gudang'];
        $barang->supplier = $request['supplier'];
        $barang->status = 'Baru';
        $barang->jumlah = 0;
        $barang->save();

        return redirect('barang');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = Barang::where('id', $id)->first();
        $gudang = Gudang::all();
        $satuan = Satuan::all();
        $supplier = Supplier::all();
        return view('barang.edit',compact('barang','gudang', 'satuan', 'supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $barang = Barang::where('id', $id)->first();
        $barang->kode = $request['kode'];
        $barang->nama = $request['nama'];
        $barang->jenis = $request['jenis'];
        $barang->harga = $request['nominal'];
        $barang->satuan = $request['satuan'];
        $barang->id_gudang = $request['gudang'];
        $barang->supplier = $request['supplier'];
        $barang->status = 'Diupdate';
        $barang->update();

        return redirect('barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\RawProduct;
use App\Warehouse;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stock =Stock::all();
        return view('stock.index', compact('stock'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $warehouse = Warehouse::all();
        $rawproduct = RawProduct::all();
        return view('stock.baru',compact('warehouse','rawproduct'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stock = new Stock;
        $stock->qty = $request['qty'];
        $stock->actual_price = $request['actual_price'];
        $stock->date_in = $request['date_in'];
        $stock->date_out = $request['date_out'];
        $stock->warehouse_id = $request['warehouse_id'];
        $stock->rawproduct_id = $request['rawproduct_id'];
        $stock->save();

        return redirect('stock');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stock =Stock::find($id);
        return view('stock.edit', compact('stock'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stock =Stock::find($id);
        $rawproduct = RawProduct::all();
        $warehouse = Warehouse::all();
        $stock->qty = $request['qty'];
        $stock->actual_price = $request['actual_price'];
        $stock->date_in = $request['date_in'];
        $stock->date_out = $request['date_out'];
        $stock->warehouse_id = $request['warehouse_id'];
        $stock->rawproduct_id = $request['rawproduct_id'];
        $stock->update();

        return redirect('stock');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RawProduct extends Model
{
    protected $table = 'rawproducts';
}

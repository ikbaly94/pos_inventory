<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';

    public function gudang(){
    	return $this->belongsTo(\App\Gudang::class, 'id_gudang');
    }

    public function transaksi()
    {
    	return $this->hasMany('App\Transaksi');
    }
}

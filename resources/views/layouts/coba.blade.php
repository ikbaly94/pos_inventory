<!DOCTYPE html>
<html lang="en">
<head>

     <title>Logistik - Landing Page Template</title>
<!-- 
Hydro Template 
http://www.templatemo.com/tm-509-hydro
-->
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

     <link rel="stylesheet" href="{{asset('tamp/css/bootstrap.min.css')}}">
     <link rel="stylesheet" href="{{asset('tamp/css/magnific-popup.css')}}">
     <link rel="stylesheet" href="{{asset('tamp/css/font-awesome.min.css')}}">

     <!-- MAIN CSS -->
     <link rel="stylesheet" href="{{asset('tamp/css/templatemo-style.css')}}">
</head>
<body>

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">
               <span class="spinner-rotate"></span>
          </div>
     </section>


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="index.html" class="navbar-brand">Logistik</a>
               </div>

               <!-- MENU LINKS -->
               
               <div class="collapse navbar-collapse">
                         <!-- Left Side Of Navbar -->
     
                         <!-- Right Side Of Navbar -->
                         <ul class="nav navbar-nav navbar-nav-first">
                             <!-- Authentication Links -->
                             @guest
                                 <li>
                                     <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                 </li>
                                 <!-- <li>
                                     <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                 </li> -->
                             @else
                                 @php $name = Auth::user()->level @endphp
                                 
                                 @if ($name == 1)
                                 <li>
                                     <a class="smoothScroll" href="{{route('gudang.index')}}">Gudang</a>
                                 </li>
                                 <li>
                                     <a class="smoothScroll" href="{{route('supplier.index')}}">Supplier</a>
                                 </li>
                                 <li>
                                     <a class="smoothScroll" href="{{route('satuan.index')}}">Satuan</a>
                                 </li>
                                 <li>
                                     <a class="smoothScroll" href="{{route('barang.index')}}">Barang</a>
                                 </li>
                                 <li>
                                     <a class="smoothScroll" href="{{route('transaksi.index')}}">Transaksi</a>
                                 </li>
                                 @else
                                 <li>
                                     <a class="smoothScroll" href="{{route('barang.index')}}">Barang</a>
                                 </li>
                                 <li>
                                     <a class="smoothScroll" href="{{route('transaksi.index')}}">Transaksi</a>
                                 </li>
                                 @endif
                                 
                           <li class="nav-item dropdown">
                                     <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                         {{ Auth::user()->name }} <span class="caret"></span>
                                     </a>
                           </li>
                                        
                         <li class="section-btn"><a href="#" data-toggle="modal" data-target="#modal-form">Sign in / Join</a></li>
                                     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                         <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();">
                                             {{ __('Logout') }}
                                         </a>
     
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                             @csrf
                                         </form>
                                     </div>
                                 </li>
                             @endguest
                         </ul>
                     

                    <ul class="nav navbar-nav navbar-right">
                         <li class="section-btn"><a href="#" data-toggle="modal" data-target="#modal-form">Sign in / Join</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <!-- HOME -->



     <!-- BLOG -->


     <!-- WORK -->

     <!-- CONTACT -->
     
     <section id="contact" data-stellar-background-ratio="0.5">
         <div><label></label></div>
         <div><label></label></div>
         <div><label></label></div>
        <main class="py-4">
            @yield('content')
        </main>
     </section>
     <section id="contact" data-stellar-background-ratio="0.5">
         <div><label></label></div>
         <div><label></label></div>
         <div><label></label></div>
        <main class="py-4">
        </main>
     </section>
     <section id="contact" data-stellar-background-ratio="0.5">
         <div><label></label></div>
         <div><label></label></div>
         <div><label></label></div>
        <main class="py-4">
        </main>
     </section>
     <section id="contact" data-stellar-background-ratio="0.5">
        <main class="py-4">
        </main>
     </section>



     <!-- MODAL -->
     <section class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
               <div class="modal-content modal-popup">

                    <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>

                    <div class="modal-body">
                         <div class="container-fluid">
                              <div class="row">

                                   <div class="col-md-12 col-sm-12">
                                        <div class="modal-title">
                                             <h2>Hydro Co</h2>
                                        </div>

                                        <!-- NAV TABS -->
                                        <ul class="nav nav-tabs" role="tablist">
                                             <li><a href="#sign_in" aria-controls="sign_in" role="tab" data-toggle="tab">Sign In</a></li>
                                             <li class="active"><a href="#sign_up" aria-controls="sign_up" role="tab" data-toggle="tab">Create an account</a></li>
                                        </ul>

                                        <!-- TAB PANES -->
                                        <div class="tab-content">
                                             <div role="tabpanel" class="tab-pane fade in active" id="sign_up">
                                                  <form action="#" method="post">
                                                       <input type="text" class="form-control" name="name" placeholder="Name" required>
                                                       <input type="telephone" class="form-control" name="telephone" placeholder="Telephone" required>
                                                       <input type="email" class="form-control" name="email" placeholder="Email" required>
                                                       <input type="password" class="form-control" name="password" placeholder="Password" required>
                                                       <input type="submit" class="form-control" name="submit" value="Submit Button">
                                                  </form>
                                             </div>

                                             <div role="tabpanel" class="tab-pane fade in" id="sign_in">
                                             <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                             @csrf
                                                       <!-- <input type="email" class="form-control" name="email" placeholder="Email" required> -->
                                                  <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required >
                                                       
                                                  @if ($errors->has('email'))
                                                       <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                       </span>
                                                  @endif
                                                  <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

                                                  @if ($errors->has('password'))
                                                       <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                       </span>
                                                  @endif
                                                       <!-- <input type="password" class="form-control" name="password" placeholder="Password" required> -->
                                                       <input type="submit" class="form-control" name=" {{ __('Login') }}" value="{{ __('Login') }}">
                                                       <a href="https://www.facebook.com/templatemo">Forgot your password?</a>
                                                  </form>
                                             </div>
                                        </div>
                                   </div>

                              </div>
                         </div>
                    </div>

               </div>
          </div>
     </section>

     <!-- SCRIPTS -->
     <script src="{{asset('tamp/js/jquery.js')}}"></script>
     <script src="{{asset('tamp/js/bootstrap.min.js')}}"></script>
     <script src="{{asset('tamp/js/jquery.stellar.min.js')}}"></script>
     <script src="{{asset('tamp/js/jquery.magnific-popup.min.js')}}"></script>
     <script src="{{asset('tamp/js/smoothscroll.js')}}"></script>
     <script src="{{asset('tamp/js/custom.js')}}"></script>

</body>
</html>
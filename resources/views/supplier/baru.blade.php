@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Data Supplier</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('supplier.store')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">Nama</label>

                            <div class="col-md-10">
                            <input type="text" name="nama" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">Addres</label>

                            <div class="col-md-10">
                            <input type="text" name="addres" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">Phone</label>

                            <div class="col-md-10">
                            <input type="text" name="phone" class="form-control" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">Pic</label>

                            <div class="col-md-10">
                            <input type="text" name="pic" class="form-control" required="">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Tambah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

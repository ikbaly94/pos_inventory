@extends('layouts.app')

@section('content')
<div class="content">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                        <a href="{{route('stock.create')}}" class="btn btn-danger">Tambah</a> 
                            </div>
                            <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                               <thead><tr>
                                  <td><b>Nama</th>
                                  <td><b>Code</th>
                                  <td><b>Location</th>
                                  <td><b>User Id</th>
                                  <td><b>Action</th>
                                </tr>
                              </thead>
                                <tbody class="table table-striped bg-info">
                                  @foreach ($stock as $g)
                                  <tr>
                                    <td>
                                      {{$g->qty}}
                                    </td>
                                    <td>
                                      {{$g->actual_price}}
                                    </td>
                                    <td>
                                      {{$g->date_in}}
                                    </td>
                                    <td>
                                      {{$g->date_out}}
                                    </td>
                                    <td>
                                      <a href="{{route('stock.edit',$g->id)}}" class="btn btn-warning">Edit</a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ubah Data Satuan</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('stock.update', $stock->id)}}">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PUT">

                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">Nama</label>

                            <div class="col-md-10">
                            <input type="text" name="qty" class="form-control" required="" value="{{$stock->qty}}">
                            </div>
                            <label for="password" class="col-md-2 col-form-label text-md-right">Code</label>
                            <div class="col-md-10">
                            <input type="text" name="actual_price" class="form-control" required="" value="{{$stock->actual_price}}">
                            </div>
                            <label for="password" class="col-md-2 col-form-label text-md-right">Location</label>
                            <div class="col-md-10">
                            <input type="text" name="date_in" class="form-control" required="" value="{{$stock->date_in}}">
                            </div>
                            <label for="password" class="col-md-2 col-form-label text-md-right">User Id</label>

                            <div class="col-md-10">
                            <input type="text" name="date_out" class="form-control" required="" value="{{$stock->date_out}}">
                            </div>
                            <label for="password" class="col-md-2 col-form-label text-md-right">Warehouse Id</label>

                            <div class="col-md-10">
                            <input type="text" name="warehoude_id" class="form-control" required="" value="{{$stock->warehoude_id}}">
                            </div>
                            <label for="password" class="col-md-2 col-form-label text-md-right">Raw Product Id</label>

                            <div class="col-md-10">
                            <input type="text" name="rawproduct_id" class="form-control" required="" value="{{$stock->rawproduct_id}}">
                            </div>
                           
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-warning">
                                    Ubah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

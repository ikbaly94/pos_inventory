@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Data Satuan</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('stock.store')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="text" class="col-md-2 col-form-label text-md-right">Qty</label>
                            <div class="col-md-10">
                            <input type="text" name="qty" class="form-control" required="">
                            </div>
                            <label for="text" class="col-md-2 col-form-label text-md-right">Actual Price</label>
                            <div class="col-md-10">
                            <input type="text" name="actual_price" class="form-control" required="">
                            </div>
                            <label for="text" class="col-md-2 col-form-label text-md-right">Date IN</label>
                            <div class="col-md-10">
                            <input type="text" name="date_in" class="form-control" required="">
                            </div>
                            <label for="text" class="col-md-2 col-form-label text-md-right">Date Out</label>
                            <div class="col-md-10">
                            <input type="text" name="date_out" class="form-control" required="">
                            </div>

                            <label for="text" class="col-md-2 col-form-label text-md-right">warehouse Id</label>
                            <div class="col-md-10">
                                <select class="form-control" name="warehouse_id">
                                    @foreach ($warehouse as $sa )
                                    <option value="{{$sa->id}}">{{$sa->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <label for="text" class="col-md-2 col-form-label text-md-right">Raw Product Id</label>
                            <div class="col-md-10">
                                <select class="form-control" name="rawproduct_id">
                                    @foreach ($rawproduct as $sa )
                                    <option value="{{$sa->id}}">{{$sa->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Tambah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

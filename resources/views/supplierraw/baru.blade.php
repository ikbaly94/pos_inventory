@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Data Supplier</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('supplierraw.store')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="text" class="col-md-2 col-form-label text-md-right">Supplier id</label>

                            <div class="col-md-10">
                                <select class="form-control" name="supplier_id">
                                    @foreach ($supplier as $sup )
                                    <option value="{{$sup->id}}">{{$sup->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-md-2 col-form-label text-md-right">Raw Product</label>
                            <div class="col-md-10">
                                <select class="form-control" name="rawproduct_id">
                                    @foreach ($rawproduct as $raw )
                                    <option value="{{$raw->id}}">{{$raw->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Tambah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

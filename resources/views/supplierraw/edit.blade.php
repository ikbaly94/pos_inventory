@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Ubah Data Supplier</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('supplierraw.update', $supplierraw->id)}}">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PUT">
                        
                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">supplier_id</label>

                            <div class="col-md-10">
                            <input type="text" name="supplier_id" class="form-control" required="" value="{{$supplierraw->supplier_id}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-2 col-form-label text-md-right">rawproduct_id</label>

                            <div class="col-md-10">
                            <input type="text" name="rawproduct_id" class="form-control" required="" value="{{$supplierraw->rawproduct_id}}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-warning">
                                    Ubah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="col-md-12 col-sm-12">
                         <div class="section-title">
                              <h2>Data Supplier</h2>
                              <span class="line-bar"></span>
                         </div>
                    </div>
                    <div class="float-right">
                      <table>
                        <td></td>
                        <td></td>
                        <td>
                        <a href="{{route('supplierraw.create')}}" class="btn btn-danger">Tambah</a> </td>
                        </table>
                    </div>

                </div>

                <div class="card-body">
                    <table class="table table-striped bg-info">
                      <thead>
                        <tr>
                          <td><b>Nama</b></td>
                          <td><b>Addres</b></td>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($supplierraw as $g)
                        <tr>
                          <td>
                            {{$g->supplier_id}}
                          </td>
                          <td>
                            {{$g->rawproduct_id}}
                          </td>
                          <td>
                            <a href="{{route('supplierraw.edit',$g->id)}}" class="btn btn-warning">Ubah</a>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

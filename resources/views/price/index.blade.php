@extends('layouts.app')

@section('content')
<div class="content">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                        <a href="{{route('price.create')}}" class="btn btn-danger">Tambah</a> 
                            </div>
                            <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                               <thead><tr>
                                  <td><b>Date Start</th>
                                  <td><b>Date Finish</th>
                                  <td><b>Raw Product Id</th>
                                  <td><b>Action</th>
                                </tr>
                              </thead>
                                <tbody class="table table-striped bg-info">
                                  @foreach ($price as $g)
                                  <tr>
                                    <td>
                                      {{$g->date_start}}
                                    </td>
                                    <td>
                                      {{$g->date_finish}}
                                    </td>
                                    <td>
                                      {{$g->rawproduct_id}}
                                    </td>
                                    <td>
                                      <a href="{{route('price.edit',$g->id)}}" class="btn btn-warning">Edit</a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection

@extends('layouts.app')

@section('content')
<div class="content">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                        <a href="{{route('menu.create')}}" class="btn btn-danger">Tambah</a> 
                            </div>
                            <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                               <thead><tr>
                                  <td><b>Name</th>
                                  <td><b>Code</th>
                                  <td><b>Role_id</th>
                                  <td><b>Action</th>
                                </tr>
                              </thead>
                                <tbody class="table table-striped bg-info">
                                  @foreach ($menu as $g)
                                  <tr>
                                    <td>
                                      {{$g->nama}}
                                    </td>
                                    <td>
                                      {{$g->code}}
                                    </td>
                                    <td>
                                      {{$g->role_id}}
                                    </td>
                                    <td>
                                      <a href="{{route('menu.edit',$g->id)}}" class="btn btn-warning">Edit</a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection

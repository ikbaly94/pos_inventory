@extends('layouts.app')

@section('content')
<div class="content">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Table</strong>
                                <a href="{{route('employee.create')}}" class="btn btn-danger">Tambah</a> 
                            </div>
                            <div class="card-body">
                            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                               <thead><tr>
                                  <td><b>Name</th>
                                  <td><b>Department</th>
                                  <td><b>Divition</th>
                                  <td><b>Position</th>
                                  <td><b>user_id</th>
                                  <td><b>Action</th>
                                </tr>
                              </thead>
                                <tbody class="table table-striped bg-info">
                                  @foreach ($employee as $g)
                                  <tr>
                                    <td>
                                      {{$g->name}}
                                    </td>
                                    <td>
                                      {{$g->department}}
                                    </td>
                                    <td>
                                      {{$g->divition}}
                                    </td>
                                    <td>
                                      {{$g->position}}
                                    </td>
                                    <td>
                                      {{$g->user_id}}
                                    </td>
                                    <td>
                                      <a href="{{route('employee.edit',$g->id)}}" class="btn btn-warning">Edit</a>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .animated -->
        </div><!-- .content -->
@endsection

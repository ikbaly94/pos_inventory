@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Data Satuan</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('rawproduct.store')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="text" class="col-md-2 col-form-label text-md-right">Name</label>
                            <div class="col-md-10">
                            <input type="text" name="name" class="form-control" required="">
                            </div>
                            <label for="text" class="col-md-2 col-form-label text-md-right">role_id</label>
                            <div class="col-md-10">
                                <select class="form-control" name="unit_id">
                                    @foreach ($unit as $sa )
                                    <option value="{{$sa->id}}">{{$sa->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Tambah
                                </button>
                                <a href="{{ URL::previous() }}" class="btn btn-danger">Batal</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

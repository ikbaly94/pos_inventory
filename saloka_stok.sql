-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Sep 2019 pada 09.30
-- Versi server: 10.1.39-MariaDB
-- Versi PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saloka_stok`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supplier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_gudang` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nominal` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `barangs`
--

INSERT INTO `barangs` (`id`, `kode`, `nama`, `jenis`, `supplier`, `satuan`, `harga`, `jumlah`, `status`, `id_gudang`, `created_at`, `updated_at`, `nominal`) VALUES
(1, 'BRG-001-001', 'PC', 'IT dan Elekronik', 'UD Banyak Logistik', 'unit', 5000000, 5, 'Diupdate', 1, '2019-09-11 19:02:19', '2019-09-12 00:28:02', 25000000),
(2, 'BRG-001-002', 'Plastik', 'Dapur', 'CV Maju Bersama', 'pack', 40000, 1, 'Diupdate', 2, '2019-09-11 19:03:11', '2019-09-11 23:09:05', 40000),
(3, 'BRG-001-003', 'Kabel', 'Elektronik', 'CV Maju Bersama', 'meter', 2500, 0, 'Baru', 3, '2019-09-11 19:04:36', '2019-09-11 19:04:36', 0),
(4, 'BRG-001-001A', 'Lampu', 'IT dan Elekronik', 'CV Maju Bersama', 'pcs', 10000, 3, 'Diupdate', 1, '2019-09-11 23:31:44', '2019-09-12 00:24:56', 30000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gudangs`
--

CREATE TABLE `gudangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `gudangs`
--

INSERT INTO `gudangs` (`id`, `kode`, `nama`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'GDG-01-01', 'Gudang A1', 2, '2019-09-11 18:56:55', '2019-09-12 00:07:45'),
(2, 'GDG-01-02', 'Gudang B1', 3, '2019-09-11 18:57:21', '2019-09-11 18:57:21'),
(3, 'GDG-01-03', 'Gudang C1', 4, '2019-09-11 18:57:54', '2019-09-11 18:57:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_10_121952_create_satuans_table', 1),
(4, '2019_09_10_122100_create_suppliers_table', 1),
(5, '2019_09_10_122138_create_gudangs_table', 1),
(6, '2019_09_10_122345_create_barangs_table', 1),
(7, '2019_09_10_122417_create_transaksis_table', 1),
(8, '2019_09_12_054015_add_nominal_to_barangs', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuans`
--

CREATE TABLE `satuans` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `satuans`
--

INSERT INTO `satuans` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'gram', '2019-09-11 18:59:52', '2019-09-12 00:19:30'),
(2, 'Dus', '2019-09-11 19:00:06', '2019-09-11 19:00:06'),
(3, 'kilogram', '2019-09-11 19:00:17', '2019-09-11 19:00:17'),
(4, 'pack', '2019-09-11 19:00:24', '2019-09-11 19:00:24'),
(5, 'pcs', '2019-09-11 19:00:32', '2019-09-11 19:00:32'),
(6, 'unit', '2019-09-11 19:00:40', '2019-09-11 19:00:40'),
(7, 'ons', '2019-09-11 19:01:09', '2019-09-11 19:01:09'),
(8, 'meter', '2019-09-11 19:01:30', '2019-09-11 19:01:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `suppliers`
--

INSERT INTO `suppliers` (`id`, `kode`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'SUPL 442', 'PT Wijaya Kusuma', '2019-09-11 18:58:15', '2019-09-12 00:14:57'),
(2, 'SUPL 443', 'CV Maju Bersama', '2019-09-11 18:58:30', '2019-09-11 18:58:30'),
(3, 'SUPL 444', 'Koperasi Makmur Bersama', '2019-09-11 18:58:58', '2019-09-11 18:58:58'),
(4, 'SUPL 4423', 'UD Banyak Logistik', '2019-09-11 18:59:37', '2019-09-11 18:59:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksis`
--

CREATE TABLE `transaksis` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `klien` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_barang` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaksis`
--

INSERT INTO `transaksis` (`id`, `jenis`, `qty`, `klien`, `id_barang`, `created_at`, `updated_at`) VALUES
(2, '1', 1, 'PT Wijaya Kusuma', 1, '2019-09-11 21:12:23', '2019-09-11 21:12:23'),
(3, '1', 2, 'PT Wijaya Kusuma', 1, '2019-09-11 21:16:04', '2019-09-11 21:16:04'),
(4, '3', 1, 'Div HRD', 1, '2019-09-11 21:19:50', '2019-09-11 21:19:50'),
(5, '1', 2, 'UD Banyak Logistik', 2, '2019-09-11 21:22:07', '2019-09-11 21:22:07'),
(6, '1', 2, 'CV Maju Bersama', 2, '2019-09-11 21:31:01', '2019-09-11 21:31:01'),
(7, '3', 2, 'Bag Konsumsi', 2, '2019-09-11 21:33:11', '2019-09-11 21:33:11'),
(8, '1', 1, 'UD Banyak Logistik', 1, '2019-09-11 21:45:17', '2019-09-11 21:45:17'),
(9, '2', 1, 'UD Banyak Logistik', 1, '2019-09-11 21:57:25', '2019-09-11 21:57:25'),
(10, '1', 1, 'UD Banyak Logistik', 1, '2019-09-11 23:05:28', '2019-09-11 23:05:28'),
(11, '2', 1, 'CV Maju Bersama', 2, '2019-09-11 23:09:05', '2019-09-11 23:09:05'),
(12, '1', 1, 'UD Banyak Logistik', 4, '2019-09-11 23:32:57', '2019-09-11 23:32:57'),
(13, '1', 1, 'UD Banyak Logistik', 4, '2019-09-11 23:33:24', '2019-09-11 23:33:24'),
(14, '1', 1, 'CV Maju Bersama', 4, '2019-09-11 23:38:20', '2019-09-11 23:38:20'),
(15, '1', 1, 'UD Banyak Logistik', 1, '2019-09-11 23:45:04', '2019-09-11 23:45:04'),
(16, '1', 1, 'UD Banyak Logistik', 1, '2019-09-12 00:28:02', '2019-09-12 00:28:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kepala Gudang', 'ka@mail.com', '$2y$10$L5gmTH/QNtDcEAFf97lCPu8TTpvJb9Hn/74S6i3YxscBUIqOsgsXS', 1, 'OGJ7cFamlZEVM9B85f35xmvsnolPiQtW4ApTpte8nTqxTXacJO3Hq9sEYLkj', '2019-09-11 18:53:37', '2019-09-11 18:53:37'),
(2, 'Admin Gudang 1', 'gdg1@mail.com', '$2y$10$L5gmTH/QNtDcEAFf97lCPu8TTpvJb9Hn/74S6i3YxscBUIqOsgsXS', 2, 'GnkOiMtaWykWU4E7inhIK5QJKQZuoR0lyOlRj1CKOX4TUOZ5DZs7ICqhu8ye', '2019-09-11 18:53:37', '2019-09-11 18:53:37'),
(3, 'Admin Gudang 2', 'gdg2@mail.com', '$2y$10$L5gmTH/QNtDcEAFf97lCPu8TTpvJb9Hn/74S6i3YxscBUIqOsgsXS', 2, 'TGKOt9QH61MUboh1Ed5iB9tIThpqzGuiEpafy8ZEuyNXg6nKvaZqYoqH7zy1', '2019-09-11 18:53:37', '2019-09-11 18:53:37'),
(4, 'Admin Gudang 3', 'gdg3@mail.com', '$2y$10$L5gmTH/QNtDcEAFf97lCPu8TTpvJb9Hn/74S6i3YxscBUIqOsgsXS', 2, 'hv2KxsHPfYQ7zZ8onGQ62Y9xl4LVWVM5KR7CkR4lW8ngHmoD1IWXBuuc4vSt', '2019-09-11 18:53:37', '2019-09-11 18:53:37');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barangs_id_gudang_foreign` (`id_gudang`);

--
-- Indeks untuk tabel `gudangs`
--
ALTER TABLE `gudangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gudangs_user_id_foreign` (`user_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `satuans`
--
ALTER TABLE `satuans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksis_id_barang_foreign` (`id_barang`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `gudangs`
--
ALTER TABLE `gudangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `satuans`
--
ALTER TABLE `satuans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barangs`
--
ALTER TABLE `barangs`
  ADD CONSTRAINT `barangs_id_gudang_foreign` FOREIGN KEY (`id_gudang`) REFERENCES `gudangs` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `gudangs`
--
ALTER TABLE `gudangs`
  ADD CONSTRAINT `gudangs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transaksis`
--
ALTER TABLE `transaksis`
  ADD CONSTRAINT `transaksis_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barangs` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
